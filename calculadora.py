import os


def clear_console():
    command = 'clear'
    if os.name in ('nt', 'dos'):  # Si el equipo corre Windows usa comando 'cls'
        command = 'cls'
    os.system(command)


class Operaciones:
    def __init__(self):  # Declaración para crear un objeto que va a almacenar las líneas de operaciones
        self.operadores = []

    def agregar_operador(self, operacion, resultado):  # Método para agregar una lista de comandos al objeto creado
        self.operadores.append(operacion + ' = ' + str(resultado))

    def mostrar_resultado(self):  # se encarga de mantener ordenado la línea de comandos, mostrando los anteriores
        cont = 0
        for i in self.operadores:
            print(f'command_line[{cont}]:', i)
            cont += 1

    def tamano_objeto(
            self):  # permite obtener el número de objetos almacenados para visualizar en la entrada de cada línea
        return len(self.operadores)


def encontrar_posiciones(cadena_entrada):  # Función que se encarga de extraer los operadores presentes en la cadena
    pos_def = []
    operadores = ['^', '/', '*', '+', '-']
    for operador in operadores:
        cadena1 = cadena_entrada
        pos1 = 0
        pos2 = 0
        pos = []
        while cadena1.find(operador) > -1:
            pos1 = cadena1.find(operador)
            pos2 += pos1
            if operador == '-' and pos2 != 0:  # Algoritmo que extrae el operador '-' (menos) pero conserva el número
                # si es negativo
                try:
                    num = float(cadena_entrada[pos2 - 1])
                    pos.append(pos2)
                except:
                    pass
            elif operador != '-':
                pos.append(pos2)
            pos1 += 1
            cadena1 = cadena1[pos1:]  # Permite recortar la cadena para extraer posición de operadores ordenadamente
            pos2 += 1
        pos_def.append(pos)
    return pos_def


def extraer_numeros(
        cadena_entrada):  # Función que permite extraer una lista de los números contenidos en la cadena, decimales, 
    # enteros, positivos y negativos
    valido = True
    lista_numeros = []
    """El siguiente árbol de operaciones fracciona la cadena de caracteres con números y operaciones
    para obtener los números disponibles para operación, realizando la división en cadenas según el
    operador, comenzando con la potencia(^), después la división(/), sigue con la multiplicación (*),
    continua con la suma(+) y finalizando con la resta(-)"""
    lista_entrada = cadena_entrada.split('^')
    for l0 in lista_entrada:
        try:
            lista_numeros.append(float(l0))
        except:
            l0 = l0.split('/')
            for l1 in l0:
                try:
                    lista_numeros.append(float(l1))
                except:
                    l1 = l1.split('*')
                    for l2 in l1:
                        try:
                            lista_numeros.append(float(l2))
                        except:
                            l2 = l2.split('+')
                            for l3 in l2:
                                try:
                                    lista_numeros.append(float(l3))
                                except:
                                    l3 = l3.split('-')
                                    for l4 in l3:
                                        try:
                                            lista_numeros.append(float(l4))
                                        except:
                                            valido = False
    if valido:
        return lista_numeros, valido
    else:
        return [], valido


def reconstruir_lista(numeros, operadores):  # Crea una lista con cada operador y número presente en la cadena
    simbolo_operador = ''
    longitud = 0
    operandos_desordenados = {}
    count = 0
    for listaOperador in operadores:
        longitud += len(listaOperador)
        if count == 0:
            simbolo_operador = '^'
        elif count == 1:
            simbolo_operador = '/'
        elif count == 2:
            simbolo_operador = '*'
        elif count == 3:
            simbolo_operador = '+'
        elif count == 4:
            simbolo_operador = '-'
        for item in listaOperador:
            operandos_desordenados[item] = simbolo_operador  # Crea un diccionario con los operadores y su posicion
            # desordenada
        count += 1
    longitud += len(numeros)
    operandos_orden = list(operandos_desordenados.keys())
    nuevo_orden = [i for i in range(longitud) if i % 2 != 0]  # Crea una lista para ordenar los operadores a ubicar
    operandos_orden.sort()  # Ordena la lista de operadores
    count = 0
    lista_numeros = numeros
    """Se insertan los operadores en los espacios entre números"""
    for elemento in operandos_orden:
        lista_numeros.insert(nuevo_orden[count], operandos_desordenados[elemento])
        count += 1
    return lista_numeros


def calcular(lista_elementos):
    """Emplea una función que realiza operaciones matemáticas para operar los dos números que están a la izquierda
    y derecha del operador, priorizando siempre la potencia, la división, la multiplicación, la resta y la suma
    """
    iteracion = lista_elementos
    while len(iteracion) > 1:
        count = 0
        for elemento in iteracion:
            if iteracion[count] == '^':
                nuevo_valor = iteracion[count - 1] ** iteracion[count + 1]
                eliminar = count - 1
                for i in range(3):
                    del iteracion[eliminar]
                iteracion.insert(eliminar, nuevo_valor)
            count += 1
        count = 0
        for elemento in iteracion:
            if iteracion[count] == '/':
                nuevo_valor = iteracion[count - 1] / iteracion[count + 1]
                eliminar = count - 1
                for i in range(3):
                    del iteracion[eliminar]
                iteracion.insert(eliminar, nuevo_valor)
            count += 1
        count = 0
        for elemento in iteracion:
            if iteracion[count] == '*':
                nuevo_valor = iteracion[count - 1] * iteracion[count + 1]
                eliminar = count - 1
                for i in range(3):
                    del iteracion[eliminar]
                iteracion.insert(eliminar, nuevo_valor)
            count += 1
        count = 0
        for elemento in iteracion:
            if iteracion[count] == '-':
                nuevo_valor = iteracion[count - 1] - iteracion[count + 1]
                eliminar = count - 1
                for i in range(3):
                    del iteracion[eliminar]
                iteracion.insert(eliminar, nuevo_valor)
            count += 1
        count = 0
        for elemento in iteracion:
            if iteracion[count] == '+':
                nuevo_valor = iteracion[count - 1] + iteracion[count + 1]
                eliminar = count - 1
                for i in range(3):
                    del iteracion[eliminar]
                iteracion.insert(eliminar, nuevo_valor)
            count += 1
    return iteracion


def run():
    continuar = 'Y'
    lista_resultados = Operaciones()
    while continuar == 'Y' or continuar == 'y':
        clear_console()
        lista_resultados.mostrar_resultado()
        entrada = "command_line[{0}]: ".format(lista_resultados.tamano_objeto())
        cadena = input(entrada)
        separacion, validar = extraer_numeros(cadena)
        if validar:
            posicion = encontrar_posiciones(cadena)
            lista_completa = reconstruir_lista(separacion, posicion)
            try:
                total = calcular(lista_completa)
                if total[0] == int(total[0]):  # Simplifica la vista del resultado eliminado decimales innecesarios
                    total_f = int(total[0])
                else:
                    total_f = round(total[0], 4)
                lista_resultados.agregar_operador(cadena, total_f)
                clear_console()
                lista_resultados.mostrar_resultado()
            except ZeroDivisionError:
                print("Error en división por Cero")
            except OverflowError:
                print("Número tiende a infinito")
        else:
            print("valores Inválidos")
        continuar = input("¿Desea continuar(y)?")
    clear_console()
    lista_resultados.mostrar_resultado()
    print("Gracias por utilizar la calculadora básica de jhonjoya@gmail.com")


if __name__ == '__main__':
    run()
